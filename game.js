/*
 * CMPT 120
 * Project #5
 * Tyler Galske
 * 11/24/15
 *
 * crash site   jungle		village
 * beach	    desert		river
 * plains		mountain	cliffs
 * 				wolf den
 */

var player = {
	currentLocation: 'desert',
	currentScore: 5,
	inventory: [],
	breadcrumbTrail: []
}

function Location(name, description, item, visited) {
	this.name = name;
	this.description = description;
	this.item = item;
	this.visited = false;
}

Location.prototype.toString = function() {
	return "You are at the " + this.name + " - " + this.description;
}

var crashSite = new Location("Crash Site", 
	"Most of the plane is destroyed. There is debris everywhere.", "Matches" , false);
var jungle = new Location("Jungle",	"A hot and humid jungle filled with bugs." , "" , false);
var village = new Location("Village",
	"The village is filled with friendly tribespeople.", "Shotgun Shells", false);
var beach = new Location("Beach", 
	"A spectacular view with soft sand.", "Radio", false);
var desert = new Location("Desert",
	"An unforgiving environment in the center of the island.", "", true);
var river = new Location("River",
	"Small rapids allow for fishing, bathing, and drinking H2O.", "", false);
var plains = new Location ("Plains",
	"Flat land stretching for miles.", "", false);
var mountain = new Location ("Mountain",
	"A rocky mountain with snow at the peak.", "Shotgun" , false);
var cliffs = new Location ("Cliffs",
	"Watch your step. The drop seems to be 100+ feet.", "Batteries", false);
var wolfDen = new Location ("Wolf Den",
	"Enter if you dare, or if you're packing heat.", "", false);

var locations = [crashSite , jungle , village , beach , desert , river , plains , mountain , cliffs , wolfDen]; // A global array to hold locations

function Item(name, description) {
	this.name = name;
	this.description = description;
}

Item.prototype.toString = function() {
	return this.name + this.description;
}

var matches = new Item ("Matches - " , "Stay warm and fend off wolves with matches.");
var shotgunShells = new Item ("Shotgun Shells - " , "16 rounds of stopping power.");
var radio = new Item ("Radio - " , "A radio will be useful for receiving messages.");
var shotgun = new Item ("Shotgun - " , "Defend yourself against all enemies.");
var batteries = new Item ("Batteries - " , "Batteries can be used to power a radio.");


var crashSitePoints = 0;
var junglePoints = 0;
var villagePoints = 0;
var beachPoints = 0;
var riverPoints = 0;
var plainsPoints = 0;
var mountainPoints = 0;
var cliffsPoints = 0;
var wolfDenPoints = 0;

var matchesCounter = 0; //found at crash site
var shotgunShellsCounter = 0; // found at village
var radioCounter = 0; //found at beach
var shotgunCounter = 0; // found at mountain
var batteriesCounter = 0; //found at cliffs

var validCommands = 'Enter N or n to move north. <br/> Enter S or s to move south.' +
	'Enter E or e to move east. <br/> Enter W or w to move west. <br/>' +
	'If you have found an item, enter T or t to place the item in your inventory.<br/>' +
	'Enter I or i to view your inventory.' +
	'Enter L or l to look around the location to find an item.';

var breadcrumbTrail = []; // array that displays previous moves

//shows the breadcrumbTrail array
function moveHistory() {
	breadcrumbTrail.reverse(); // reverses the array to show most recent location
	document.getElementById('moves').innerHTML = breadcrumbTrail;
}

// shows the location in HTML
function showLocation(descrip) {
	document.getElementById('scene').innerHTML = descrip;
}

// displays text saying user picked up an item. example: You have picked up the matches.
function showItem(item) {
    document.getElementById('itemMsg').innerHTML = item;
}

// shows list of commands for user
function help() {
	errorMsg(validCommands); //errorMsg can be used for displaying error msgs and help text.
}

// shows the score in HTML
function showScore(points) {
    document.getElementById('score').innerHTML = points;
}

// displays an error message if invalid command is entered
function errorMsg(retry) {
    document.getElementById('errorMsg').innerHTML = retry;    
}

function inventory() {
	showInventory(player.inventory);
}

function showInventory(item) {
	document.getElementById('items').innerHTML = item;
}

function lookAround(){
	var itemFound = "You have found the ";
	
	if (player.currentLocation === 'crashSite' && matchesCounter === 1){
		showItem(itemFound + matches);
		document.getElementById('takeBtn').disabled = false;
	} else if (player.currentLocation === 'village' && shotgunShellsCounter === 1) {
		showItem(itemFound + shotgunShells);
		document.getElementById('takeBtn').disabled = false;
	} else if (player.currentLocation === 'beach' && radioCounter === 1) {
		showItem(itemFound + radio);
		document.getElementById('takeBtn').disabled = false;
	} else if (player.currentLocation === 'mountain' && shotgunCounter === 1) {
		showItem(itemFound + shotgun);
		document.getElementById('takeBtn').disabled = false;
	} else if (player.currentLocation === 'cliffs' && batteriesCounter === 1) {
		showItem(itemFound + batteries);
		document.getElementById('takeBtn').disabled = false;
	}
	else {
		showItem("No items here.");
	}
	
}

function takeItem() {
	var itemPickedUp = "You have picked up the ";
		
	if (player.currentLocation === 'crashSite' && matchesCounter === 1) {
        showItem(itemPickedUp + matches); // showItem displays the text
		matchesCounter = 2;
		player.inventory.push(" Matches");
	}
	else if (player.currentLocation === 'village' && shotgunShellsCounter === 1) {
		showItem(itemPickedUp + shotgunShells); // showItem displays the text
		shotgunShellsCounter = 2;
		player.inventory.push(" Shotgun Shells");
	}
	else if (player.currentLocation === 'beach' && radioCounter === 1) {
		showItem(itemPickedUp + radio); // showItem displays the text
		radioCounter = 2;
		player.inventory.push(" Radio");
	}
	else if (player.currentLocation === 'mountain' && shotgunCounter === 1){
		showItem(itemPickedUp + shotgun); // showItem displays the text
		shotgunCounter = 2;
		player.inventory.push(" Shotgun");
	}
	else if (player.currentLocation === 'cliffs' && batteriesCounter === 1) {
		showItem(itemPickedUp + batteries); // showItem displays the text
		batteriesCounter = 2;
		player.inventory.push(" Batteries");
	}
}




// function for user text input
function submit() {
    var isError = 'Please enter a different command. Press Help for more information.';
    var noError = ''; // replaces the error message above ^
	
	if(userInput.value === 'N' || userInput.value === 'n'){
		goNorth(); // calls the goNorth function
        errorMsg(noError);
	} else if(userInput.value === 'S' || userInput.value === 's') {
		goSouth(); // calls the goSouth function
        errorMsg(noError);
	} else if(userInput.value === 'E' || userInput.value === 'e') {
		goEast();
        errorMsg(noError);
	} else if(userInput.value === 'W' || userInput.value === 'w') {
		goWest();
        errorMsg(noError);
	} else if (userInput.value === 'T' || userInput.value === 't') {
        takeItem();
    } else if (userInput.value === 'I' || userInput.value === 'i') {
		inventory();
		errorMsg(noError);
	} else if (userInput.value === 'H' || userInput.value === 'h') {
		errorMsg(validCommands);
	} else if (userInput.value === 'P' || userInput.value === 'p') {
		moveHistory();
		errorMsg(noError);
	} else if (userInput.value === 'L' || userInput.value === 'l') {
		lookAround();
		errorMsg(noError);
	} else {
		errorMsg(isError);
	}
}

// adds 5 points to score if the location has been visited once
function pointTracker() {
	if (player.currentLocation === 'crashSite' && crashSitePoints === 0) {
        player.currentScore += 5; // add 5 points
        crashSitePoints += 1; // only run 1 time
    }    
	if (player.currentLocation === 'jungle' && junglePoints === 0) {
	   player.currentScore += 5; // add 5 points
	   junglePoints += 1; // only run 1 time
	}
	if (player.currentLocation === 'village' && villagePoints === 0) {
		player.currentScore += 5; // add 5 points
		villagePoints += 1; // only run 1 time
	}
    if (player.currentLocation === 'beach' && beachPoints === 0) {
		player.currentScore += 5; // add 5 points
		beachPoints += 1; // only run 1 time
	}
    if (player.currentLocation === 'river' && riverPoints === 0) {
		player.currentScore += 5; // add 5 points
		riverPoints += 1; //only run one time
	}
    if (player.currentLocation === 'plains' && plainsPoints ===0) {
        player.currentScore += 5; // add 5 points
        plainsPoints += 1; //only run 1 time
    }
	if (player.currentLocation === 'mountain' && mountainPoints === 0) {
		player.currentScore += 5; // add 5 points
		mountainPoints += 1; // only run one time
	}	
    if (player.currentLocation === 'cliffs' && cliffsPoints === 0){
		player.currentScore += 5; // add 5 points
		cliffsPoints += 1; // only run 1 time
	}
    if (player.currentLocation === 'wolfDen' && wolfDenPoints === 0){
		player.currentScore += 5; // add 5 points
		wolfDenPoints += 1; // only run 1 time
	}
    showScore(player.currentScore);
}

// new north button to use player object

function goNorth() {
	switch (player.currentLocation) {
		case 'crashSite':
			player.currentLocation = 'crashSite';
			crashSite();
			crashSite.visited = true;
		break;
		case 'jungle':
			player.currentLocation = 'jungle';
			jungle();
			jungle.visited = true;
		break;
		case 'village':
			player.currentLocation = 'village';
			village();
			village.visited = true;
		break;
		case 'beach':
			player.currentLocation = 'crashSite';
			crashSite();
			crashSite.visited = true;
		break;
		case 'desert':
			player.currentLocation = 'jungle';
			jungle();
			jungle.visited = true;
		break;
		case 'river':
			player.currentLocation = 'village';
			village();
			village.visited = true;
		break;
		case 'plains':
			player.currentLocation = 'beach';
			beach();
			beach.visited = true;
		break;
		case 'mountain':
			player.currentLocation = 'desert';
			desert();
			desert.visited = true;
		break;
		case 'cliffs':
			player.currentLocation = 'river';
			river();
			river.visited = true;
		break;
		case 'wolfDen':
			player.currentLocation = 'mountain';
			mountain();
			mountain.visited = true;
	}
}

function goSouth() {
    switch(player.currentLocation) {
        case 'crashSite':
            player.currentLocation = 'beach';
            beach();
			beach.visited = true;
            break;
        case 'jungle':
            player.currentLocation = 'desert';
            desert();
			desert.visited = true;
            break;
        case 'village':
            player.currentLocation = 'river';
            river();
			river.visited = true;
            break;        
        case 'beach':
            player.currentLocation = 'plains';
            plains();
			plains.visited = true;
            break;
        case 'desert':
            player.currentLocation = 'mountain';
            mountain();
			mountain.visited = true;
            break;
        case 'river':
            player.currentLocation = 'cliffs';
            cliffs();
			cliffs.visited = true;
            break;
        case 'plains':
            player.currentLocation = 'plains';
            plains();
			plains.visited = true;
            break;
        case 'mountain':
            player.currentLocation = 'wolfDen';
            wolfDen();
			wolfDen.visited = true;
            break;
        case 'cliffs':
            player.currentLocation = 'cliffs';
            cliffs();
			cliffs.visited = true;
            break;
        case 'wolfDen':
            player.currentLocation = 'wolfDen';
            wolfDen();
			wolfDen.visited = true;
            break;
    } // end switch
} // end of goSouth function


function goEast() {
    switch(player.currentLocation) {
        case 'crashSite':
            player.currentLocation = 'jungle';
            jungle();
			jungle.visited = true;
        break;
        case 'jungle':
            player.currentLocation = 'village';
            village();
			village.visited = true;
        break;
        case 'village':
            player.currentLocation = 'village';
            village();
			village.visited = true;
        break;
        case 'beach':
            player.currentLocation = 'desert';
            desert();
			desert.visited = true;
        break;
        case 'desert':
            player.currentLocation = 'river';
            river();
			river.visited = true;
        break;
        case 'river':
            player.currentLocation = 'river';
            river();
			river.visited = true;
        break;
        case 'plains':
            player.currentLocation = 'mountain';
            mountain();
			mountain.visited = true;
        break;
        case 'mountain':
            player.currentLocation = 'cliffs';
            cliffs();
			cliffs.visited = true;
        break;
        case 'cliffs':
            player.currentLocation = 'cliffs';
            cliffs();
			cliffs.visited = true;
        break;
        case 'wolfDen':
            player.currentLocation = 'wolfDen';
            wolfDen();
			wolfDen.visited = true;
        break;            
    } // end of switch
} // end goEast function


function goWest() {
    switch(player.currentLocation) {
        case 'crashSite':
            player.currentLocation = 'crashSite';
            crashSite();
			crashSite.visited = true;
        break;
        case 'jungle':
            player.currentLocation = 'crashSite';
            crashSite();
			crashSite.visited = true;
        break;
        case 'village':
            player.currentLocation = 'jungle';
            jungle();
			jungle.visited = true;
        break;
        case 'beach':
            player.currentLocation = 'beach';
            beach();
			beach.visited = true;
        break;
        case 'desert':
            player.currentLocation = 'beach';
            beach();
			beach.visited = true;
        break;
        case 'river':
            player.currentLocation = 'desert';
            desert();
			desert.visited = true;
        break;
        case 'plains':
            player.currentLocation = 'plains';
            plains();
			plains.visited = true;
        break;
        case 'mountain':
            player.currentLocation = 'plains';
            plains();
			plains.visited = true;
        break;
        case 'cliffs':
            player.currentLocation = 'mountain';
            mountain();
			mountain.visited = true;
        break;
        case 'wolfDen':
            player.currentLocation = 'wolfDen';
            wolfDen();
			wolfDen.visited = true;
        break;        
    } // end of switch
} // end of goWest function