// locations.js
/*
 * CMPT 120
 * Project #5
 * Tyler Galske
 * 11/24/15
 */
 
function crashSite() {
	breadcrumbTrail.push("Crash Site");
    showLocation(locations[0]);
    document.getElementById('northBtn').disabled = true;
	document.getElementById('southBtn').disabled = false;
	document.getElementById('eastBtn').disabled = false;
	document.getElementById('westBtn').disabled = true;
    
    if (matchesCounter === 0 || matchesCounter === 1) {
        matchesCounter = 1;
		document.getElementById('takeBtn').disabled = true;		
    }
	else if (matchesCounter === 2) { // if item has already been picked up, show nothing
		showItem("<br/>");
	}
}

function jungle() {
	breadcrumbTrail.push("Jungle");
	showLocation(locations[1]);
    
    document.getElementById('northBtn').disabled = true;
	document.getElementById('southBtn').disabled = false;
	document.getElementById('eastBtn').disabled = false;
	document.getElementById('westBtn').disabled = false;
	showItem("<br/>");
}



function village() {
	breadcrumbTrail.push("Village");
	showLocation(locations[2]);
	
	document.getElementById('northBtn').disabled = true;
	document.getElementById('southBtn').disabled = false;
	document.getElementById('eastBtn').disabled = true;
	document.getElementById('westBtn').disabled = false;
	
	if (shotgunShellsCounter === 0 || shotgunShellsCounter === 1) {
		shotgunShellsCounter = 1;
		document.getElementById('takeBtn').disabled = true;
	}
	else if (shotgunShellsCounter === 2) { // if item has already been picked up, show nothing
		showItem("<br/>");
	}
}

function beach() {
	breadcrumbTrail.push("Beach");
	showLocation(locations[3]);
	
	document.getElementById('northBtn').disabled = false;
	document.getElementById('southBtn').disabled = false;
	document.getElementById('eastBtn').disabled = false;
	document.getElementById('westBtn').disabled = true;
	
	if (radioCounter === 0 || radioCounter === 1) {
		radioCounter = 1;
		document.getElementById('takeBtn').disabled = true;
	}
	else if (radioCounter === 2) { // if item has already been picked up, show nothing
		showItem("<br/>");
	}
}

function desert() {
	breadcrumbTrail.push("Desert");
	showLocation(locations[4]);
	
	document.getElementById('northBtn').disabled = false;
	document.getElementById('southBtn').disabled = false;
	document.getElementById('eastBtn').disabled = false;
	document.getElementById('westBtn').disabled = false;
	showItem("<br/>");
}

function river() {
	breadcrumbTrail.push("River");
	showLocation(locations[5]);
	document.getElementById('northBtn').disabled = false;
	document.getElementById('southBtn').disabled = false;
	document.getElementById('eastBtn').disabled = true;
	document.getElementById('westBtn').disabled = false;
	showItem("<br/>");
}

function plains() {
	breadcrumbTrail.push("Plains");
	showLocation(locations[6]);
	
	document.getElementById('northBtn').disabled = false;
	document.getElementById('southBtn').disabled = true;
	document.getElementById('eastBtn').disabled = false;
	document.getElementById('westBtn').disabled = true;
	showItem("<br/>");
}


function mountain() {
	breadcrumbTrail.push("Mountain");
	showLocation(locations[7]);
	
	document.getElementById('northBtn').disabled = false;
	document.getElementById('southBtn').disabled = false;
	document.getElementById('eastBtn').disabled = false;
	document.getElementById('westBtn').disabled = false;
	
	if (shotgunCounter === 0 || shotgunCounter === 1) {
        shotgunCounter = 1;
		document.getElementById('takeBtn').disabled = true;
    }
	else if (shotgunCounter === 2) { // if item has already been picked up, show nothing
		showItem("<br/>");
	}
}

function cliffs() {
	breadcrumbTrail.push("Cliffs");
	showLocation(locations[8]);
	
	document.getElementById('northBtn').disabled = false
	document.getElementById('southBtn').disabled = true;
	document.getElementById('eastBtn').disabled = true;
	document.getElementById('westBtn').disabled = false;
	
	if (batteriesCounter === 0 || batteriesCounter === 1) {
		batteriesCounter = 1;
		document.getElementById('takeBtn').disabled = true;
	}
	else if (batteriesCounter === 2) {
		showItem("<br/>");
	}
}

function wolfDen() {
	breadcrumbTrail.push("Wolf Den");
	showLocation(locations[9]);
	
	document.getElementById('northBtn').disabled = false;
	document.getElementById('southBtn').disabled = true;
	document.getElementById('eastBtn').disabled = true;
	document.getElementById('westBtn').disabled = true;
	showItem("<br/>");
}

